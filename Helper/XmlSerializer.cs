﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Storage;

namespace LastFreak.Helpers
{
    /// <summary>
    /// Xml Serialisierung in UWPs
    /// </summary>
    public class XmlSerializerHelper
    {
        /// <summary>
        /// Wandelt ein Objekt in eine Xml-Datei um und speichet sie im LocalFolder ab.
        /// </summary>
        /// <param name="obj">Das zu serialisierende Objekt.</param>
        /// <param name="filename">Dateiname mit Endung(z.B "test.xml")</param>
        /// <returns>-</returns>
        public async static Task ToFile(object obj, string filename)
        {
            try
            {
                XmlSerializer x = new XmlSerializer(obj.GetType());
                var f = await ApplicationData.Current.LocalFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);
                using (Stream s = await f.OpenStreamForWriteAsync())
                {
                    StreamWriter sw = new StreamWriter(s);
                    x.Serialize(s, obj);
                    Debug.WriteLine("Created file: " + filename);
                }
            }
            catch { }
        }
        /// <summary>
        /// Wandelt ein Objekt in eine Xml-Datei um und speichet sie im LocalFolder ab.
        /// </summary>
        /// <param name="obj">Das zu serialisierende Objekt.</param>
        /// <param name="filepath">Der Speicherpfad(z.B "Ebene1/Ebene2")</param>
        /// <param name="filename">Dateiname mit Endung(z.B "test.xml")</param>
        /// <returns>-</returns>
        public async static Task ToFile(object obj, string filepath, string filename)
        {
            try
            {
                XmlSerializer x = new XmlSerializer(obj.GetType());
                StorageFolder folder = ApplicationData.Current.LocalFolder;
                foreach (string s in filepath.Split('/'))
                {
                    folder = await folder.CreateFolderAsync(s, CreationCollisionOption.OpenIfExists);
                }
                var f = await folder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);
                using (Stream s = await f.OpenStreamForWriteAsync())
                {
                    StreamWriter sw = new StreamWriter(s);
                    x.Serialize(s, obj);
                    Debug.WriteLine("Created file: " + filename);
                }
            }
            catch { }
        }
        /// <summary>
        /// Erstellt ein Objekt aus einer Xml-Datei
        /// </summary>
        /// <param name="T">Der Zielobjekttyp</param>
        /// <param name="filename">Dateiname mit Endung(z.B "test.xml")</param>
        /// <returns>Objekt aus Xml-Datei</returns>
        public async static Task<T> ToObject<T>(string filename)
        {
            try
            {
                XmlSerializer x = new XmlSerializer(typeof(T));
                var f = await ApplicationData.Current.LocalFolder.GetFileAsync(filename);
                using (StreamReader reader = new StreamReader(await f.OpenStreamForReadAsync()))
                {
                    T obj = (T)x.Deserialize(reader);
                    return obj;
                }
            }
            catch
            {
                return default(T);
            }
        }
        /// <summary>
        /// Erstellt ein Objekt aus einer Xml-Datei
        /// </summary>
        /// <typeparam name="T">Der Zielobjekttyp</typeparam>
        /// <param name="filepath">Der Speicherpfad(z.B "Ebene1/Ebene2")</param>
        /// <param name="filename">Dateiname mit Endung(z.B "test.xml")</param>
        /// <returns>Objekt aus Xml-Datei</returns>
        public async static Task<T> ToObject<T>(string filepath, string filename)
        {
            try
            {
                XmlSerializer x = new XmlSerializer(typeof(T));
                StorageFolder folder = ApplicationData.Current.LocalFolder;
                foreach (string s in filepath.Split('/'))
                {
                    folder = await folder.CreateFolderAsync(s, CreationCollisionOption.OpenIfExists);
                }
                var f = await folder.GetFileAsync(filename);
                using (StreamReader reader = new StreamReader(await f.OpenStreamForReadAsync()))
                {
                    T obj = (T)x.Deserialize(reader);
                    return obj;
                }
            }
            catch
            {
                return default(T);
            }
        }
    }
}