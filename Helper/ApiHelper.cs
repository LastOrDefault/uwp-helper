﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace LastFreak.Helpers
{
    /// <summary>
    /// Hilfsklasse für JSON-APIs in UWPs
    /// </summary>
    public class ApiHelper
    {
        /// <summary>
        /// Ruft eine JSON-Datei von der Url ab und wandelt sie in das gewünschte Objekt um.
        /// </summary>
        /// <typeparam name="T">Objekttyp der Rückgabe</typeparam>
        /// <param name="url">Ziel der Anfrage</param>
        /// <returns></returns>
        public static async Task<T> apiRequest<T>(string url)
        {
            try
            {
                Uri getUri = new Uri(url);
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(getUri);

                string responseString = await response.Content.ReadAsStringAsync(); //Liest JSON-String aus
                //Debug.WriteLine(responseString);
                client.Dispose();
                object responseObject = JsonConvert.DeserializeObject<T>(responseString);
                if (responseObject != null)
                    return (T)responseObject;   //Bei Erfolg.
                else
                    return default(T);  //Eventualität behandeln!!!
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error: {ex.Message} in ApiHelper.apiRequest<T>");
                return default(T);
            }
        }
        /// <summary>
        /// Ruft den JSON-String von der gewünschten Url ab. Für Testzwecke.
        /// </summary>
        /// <param name="url">Ziel der Anfrage</param>
        /// <returns></returns>
        public static async Task<string> getJsonAsString(string url)
        {
            Uri getUri = new Uri(url);
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(getUri);
            string responseString = await response.Content.ReadAsStringAsync();
            client.Dispose();
            return responseString;
        }
        /// <summary>
        /// Schickt einen JSON-String mittels POST zum Server
        /// </summary>
        /// <param name="uri">Ziel der Anfrage</param>
        /// <param name="postObject">Das Objekt für die Post-Anfrage</param>
        /// <returns>-</returns>
        public static async Task<string> Post(string uri, object postObject)
        {
            string json = JsonConvert.SerializeObject(postObject);

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.ContentType = "text/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(await httpWebRequest.GetRequestStreamAsync()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Dispose();
            }

            var httpResponse = (HttpWebResponse)await httpWebRequest.GetResponseAsync();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                httpResponse.Dispose();
                return result;
            }
        }
    }
}